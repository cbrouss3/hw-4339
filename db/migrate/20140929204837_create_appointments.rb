class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.text :complaint
      t.date :appointment_date

      t.timestamps
    end
  end
end
