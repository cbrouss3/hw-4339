# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
@patients = Patient.create([{name: 'Sam Johnson' }, {street_address: '5469 Calhoun Rd'}, {name: 'Stacy Dash'}, {street_address: '1565 MLK BLD'}, {name: 'Tom Stiles'}, {street_address: '5757 Wheeler'}, {name: 'Red Evans'}, {street_address:'3454 Lamar St'}, {name:'Jason Williams'}, {street_address: '4545 Tidwell St'}])

#Worked with Jesus Lozano  on this assignment.