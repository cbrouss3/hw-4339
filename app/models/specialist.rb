class Specialist < ActiveRecord::Base
  attr_accessible :name, :speciality
has_many :appointments

  has_many :patients, through: :appointments
end
