class Patient < ActiveRecord::Base
  attr_accessible :name, :street_address
has_many :specialist, through: :appointments

has_many :appointments

  has_many :insurances
end
