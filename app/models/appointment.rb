class Appointment < ActiveRecord::Base
  attr_accessible :appointment_date, :complaint
  belongs_to :specialist

  belongs_to :patient

  has_many :specialists

  has_many :patients



end
