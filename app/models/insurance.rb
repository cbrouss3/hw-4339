class Insurance < ActiveRecord::Base
  attr_accessible :name, :street_address
  belongs_to :patient

  has_many :patients


end
